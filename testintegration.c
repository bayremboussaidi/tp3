#include "jeu.h"
#include <stdio.h>

int main(int argc , char**argv) {
  int i , res;
  char pc;
  
  for (i = 1 ; i < argc ; i++){
    pc = hasard();
	res = comparaison(argv[i][0] , pc);
	printf("Joueur : %c\tPC : %c\tRésultat : %d\n" , argv[i][0] , pc , res);
  }
  
  return 0;
}